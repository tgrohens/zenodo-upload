# Example notebook for uploading data to Zenodo

This notebook exemplifies how to send experimental data to Zenodo using the REST API, based on the Zenodo [tutorial](https://developers.zenodo.org/#quickstart-upload).
You need to have a Zenodo API token stored in a `config.json` file, which is ignored by git as this token is secret.
